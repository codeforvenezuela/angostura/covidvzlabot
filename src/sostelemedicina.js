const fetch = require('node-fetch');
const diagnostico = require('./diagnostico');
const utils = require('./utils');
const config = require('config');

function initSession() {
  const url = config.get("sostelemed.init_session_url")
  return fetch(url, {
      method: "GET",
      headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          'App-Token': process.env.SOSTELEMED_APP_TOKEN,
          Authorization: 'user_token ' + process.env.SOSTELEMED_USER_TOKEN
      },
  });
}

function sendInfo(sessionToken, patientInfo) {
  const url = config.get("sostelemed.chatbot_url")
  console.log(patientInfo);
  return fetch(url, {
      method: "POST",
      body: JSON.stringify(patientInfo),
      headers: {
          "Content-Type": "application/json",
          'App-Token': process.env.SOSTELEMED_APP_TOKEN,
          'Session-Token': sessionToken
      },
  });
}

function killSession(sessionToken) {
  const url = config.get("sostelemed.init_session_url")
  return fetch(url, {
      method: "GET",
      headers: {
          "Content-Type": "application/json",
          'App-Token': process.env.SOSTELEMED_APP_TOKEN,
          'Session-Token': sessionToken
      },
  });
}

function handle(agent) {
  const patientInfo = getUserDetails(agent);
  var sessionToken = '';

  console.log(patientInfo);

  initSession().then((initSessionRes) => {
    return utils.handlePromise(initSessionRes);
  }).then((initSessionJson) => {
    sessionToken = initSessionJson.session_token;
    // Send info to SOSTelemed
    return sendInfo(sessionToken, patientInfo);
  }).then((sendInfoRes) => {
    if (sendInfoRes.ok || sendInfoRes.status == 207)
    {
      console.log('Success!!');
    } else {
      console.log('Fail!!');
    }
    // End session
    console.log(sessionToken);
    return killSession(sessionToken);
  }).then((killSessionRes) => {
    if (killSessionRes.ok) {
      console.log('Session ended')
    }
  }).catch((error) => {
    console.log(error);
  });

  agent.add("¡Gracias por responder! Sabemos que en estos momentos no se \
  debe estar sintiendo bien. Su situación puede relacionarse con un cuadro \
  de COVID-19. Le recomendamos comunicarse de forma gratuita con uno de nuestros \
  médicos de la Universidad Central de Venezuela a LlamadaSOS 212-3135660. \
  Disponible de lunes a domingo de 8:00 a.m. a 6:00 p.m.");
}

function getAge(ageParam) {
  return utils.toBool(ageParam) ? 65 : 0;
}

function getPhoneNumberTrimmed(number) {
  // Remove any character that isn't a number
  const allNumbers = number ? number.replace(/\D/g, "") : '';
  // Remove leading zeroes
  return allNumbers.replace(/^0+/, '');
}

function getSOSTelemedScore(medRisk, covidRisk) {
  var score = 0;

  if (medRisk == 'BAJO' && covidRisk == 'ALTO') {
    score = 13;
  } else if (medRisk == 'ALTO' && covidRisk == 'BAJO') {
    score = 11;
  } else if (medRisk == 'ALTO' && covidRisk == 'ALTO') {
    score = 18;
  } else if (medRisk == 'MEDIO' && covidRisk == 'MEDIO') {
    score = 6;
  } else {
    score = 4;
  }

  return score;
}

function getUserDetails(agent) {
  var patientInfo = {
    input: {
      source: ''
    }
  };

  //Get diagnosis
  var params;
  for (let i = 0; i < agent.contexts.length; i++) {
      if (agent.contexts[i].name === 'diagnostic') {
        params = agent.contexts[i];
          break;
      }
  }

  if (params === null) {
      console.error("Unable to fetch diagnostic context");
      return;
  }

  if (params.name != 'diagnostic') {
      console.error('Wrong context fetched!');
      return;
  }

  const request = agent.request_;
  if (
      request &&
      "body" in request &&
      "originalDetectIntentRequest" in request.body &&
      "source" in request.body.originalDetectIntentRequest
  ) {
      patientInfo.input.source = request.body.originalDetectIntentRequest.source;
  }
  
  const userParams = agent.parameters;
  const diagnosticParams = params.parameters;

  var risks = diagnostico.getDiagnosis(diagnosticParams);
  const medRisk = risks[0][0];
  const covidRisk = risks[1][0];

  console.log(diagnosticParams);
  console.log(userParams);

  // Diagnostic info
  patientInfo.input.callerid = getPhoneNumberTrimmed(userParams['phone_number']);
  patientInfo.input.score = getSOSTelemedScore(medRisk, covidRisk);
  patientInfo.input['riesgo_medico'] = medRisk;
  patientInfo.input['riesgo_covid'] = covidRisk;
  patientInfo.input['contacto-covid19'] = utils.toInt(diagnosticParams['contacto-covid19']);
  patientInfo.input['tos'] = utils.toInt(diagnosticParams.tos);
  patientInfo.input['fiebre'] = utils.toInt(diagnosticParams.fiebre);
  patientInfo.input['edad'] = getAge(diagnosticParams['mayor-60']);
  patientInfo.input.mialgia = utils.toInt(diagnosticParams['mialgia-garganta']);
  patientInfo.input['dificultad-respirar'] = utils.toInt(diagnosticParams['disnea-confusion']);

  // Begin currently deprecated / no mapping available - review with SOSTelemedicina
  patientInfo.input['escalofrio'] = 0; //utils.toInt(diagnosticParams.escalofrios);
  patientInfo.input['viajes-internacionales'] = 0;//utils.toInt(diagnosticParams['viajes-internacionales']);
  // End currently deprecated / no mapping available - review with SOSTelemedicina

  patientInfo.input.status = 'Referido';

  return patientInfo;
}

exports.handle = handle;