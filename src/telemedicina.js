const venemergencias = require('./venemergencias');
const sostelemedicina = require('./sostelemedicina');

function handle(agent) {
    //const probability = Math.random();
    // TODO: Testing SOSTelemed only
    const probability = .6;
    console.log(probability);

    if (probability < .5) {
        console.log('Redirecting to Venemergencias');
        venemergencias.handle(agent);
    } else {
        console.log('Redirecting to SOSTelemedicina');
        sostelemedicina.handle(agent);
    }
}

exports.handle = handle;