const weightsMap = new Map();
weightsMap.set('contacto-covid19', 8);
weightsMap.set('fiebre', 6);
weightsMap.set('tos', 6);
weightsMap.set('anosmia-rash', 6);
weightsMap.set('disnea-confusion', 4);
weightsMap.set('cefalea', 4);
weightsMap.set('mialgia-garganta', 2);
weightsMap.set('fatiga-hiporexia', 2);
weightsMap.set('dolor-pecho', 3);
// deprecated weights
weightsMap.set('viajes-internacionales', 0);
weightsMap.set('mialgia', 0);
weightsMap.set('dificultad-respirar', 0);
weightsMap.set('escalofrios', 0);
weightsMap.set('artralgia', 0);
weightsMap.set('rinorrea', 0);
// end deprecated covid weights

// riesgo médico weights
weightsMap.set('historia-pulmon', 2);
weightsMap.set('mayor-60', 2);
weightsMap.set('inmunidad', 2);
// begin deprecated riesgo medico weights
weightsMap.set('med-inmune', 0);
weightsMap.set('mayor-65', 0);
weightsMap.set('dificultad-respirar-medico', 0);
// end deprecated riesgo medico weights

function getWeight(resp, param) {
    if (!resp) {
        return 0;
    }
    if (resp[0] == "s" || resp[0] == "S") {
        return weightsMap.get(param);
    }
    return 0;
}

function getRiesgoInfeccion(args) {
    const total = args.reduce((a, b) => a + b, 0);

    if (total > 11) {
        return ["ALTO", total];
    }

    if (total > 7) {
        return ["MEDIO", total];
    }

    return ["BAJO", total];
}

function getRiesgoMedico(args) {
    const total = args.reduce((a, b) => a + b, 0);

    if (total > 3) {
        return ["ALTO", total];
    }

    if (total > 1) {
        return ["MEDIO", total];
    }

    return ["BAJO", total];
}

function getDiagnosis(parameters) {
    const covidRiskParamNames = [
        'contacto-covid19', 
        'fiebre',
        'tos',
        'anosmia-rash',
        'disnea-confusion',
        'cefalea',
        'mialgia-garganta',
        'fatiga-hiporexia', 
        'dolor-pecho'];

    const medRiskParamNames = [
        'historia-pulmon', 
        'mayor-60',
        'inmunidad'];

    covidRiskParams = [];
    medRiskParams = [];

    covidRiskParamNames.forEach(paramName => {
        covidRiskParams.push(
            getWeight(parameters[paramName], paramName));
    });

    medRiskParamNames.forEach(paramName => {
        medRiskParams.push(
            getWeight(parameters[paramName], paramName));
    });

    const medicalRisk = getRiesgoMedico(medRiskParams);
    const covidRisk = getRiesgoInfeccion(covidRiskParams);

    console.log(medicalRisk);
    console.log(covidRisk);

    return [medicalRisk, covidRisk];
}

exports.getDiagnosis = getDiagnosis;
exports.getWeight = getWeight;
exports.getRiesgoInfeccion = getRiesgoInfeccion;
exports.getRiesgoMedico = getRiesgoMedico;
exports.getWeight = getWeight;