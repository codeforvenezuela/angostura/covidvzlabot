function toBool(st) {
    return st && st.length > 0 && (st[0] == "s" || st[0] == "S");
}

function toInt(param) {
    return toBool(param) ? 1 : 0;
}

function handlePromise(res) {
    if (res.ok || 
        (res.status >= 0 && res.status < 300)) {
        return res.json();
    } else {
        let err = new Error(res.statusText)
        err.response = res
        throw err
    }
}

function isValidNumber(phone_number) {
    //TODO(alberto): Add valid regex
    // const regex = RegExp(
    //     "^(?:(?:00|+)58|0)(?:2(?:12|4[0-9]|5[1-9]|6[0-9]|7[0-8]|8[1-35-8]|9[1-5]|3[45789])|4(?:1[246]|2[46]))d{7}$"
    // );
    return true;
}

exports.toBool = toBool;
exports.toInt = toInt;
exports.handlePromise = handlePromise;
exports.isValidNumber = isValidNumber;