const express = require('express')

const { WebhookClient } = require('dialogflow-fulfillment')
const app = express()

const angostura = require('./angostura');
const venemergencias = require('./venemergencias');
const sostelemedicina = require('./sostelemedicina');
const telemedicina = require('./telemedicina');

app.get('/', (req, res) => res.send('online'))
app.post('/dialogflow', express.json(), (request, response) => {
    const agent = new WebhookClient({ request: request, response: response });
    console.log(agent.intent)

    // Run the proper function handler based on the matched Dialogflow intent name
    const intentMap = new Map();
    //Note: the intent names must match the ones set in Dialogflow
    intentMap.set('diagnostic', angostura.handle);
    intentMap.set('diagnostic-venemergencias', venemergencias.handle);
    intentMap.set('diagnostic-sostelemedicina', sostelemedicina.handle);
    intentMap.set('diagnostic-telemedicina', telemedicina.handle);
    agent.handleRequest(intentMap);
});

module.exports = app;