const fetch = require("node-fetch");
const diagnostico = require('./diagnostico');
const utils = require('./utils');
const config = require('config');

const qa_url = "https://testgrupov.vidaplatform.com/api/covid19/c4v"

function getUserDetails(diagnosticParams, venemParams) {
    patientInfo = {};

    //Fill in diagnostic information
    patientInfo.fever = utils.toBool(diagnosticParams.fiebre);
    patientInfo.cough = utils.toBool(diagnosticParams.tos);
    patientInfo.respiratory_distress = utils.toBool(diagnosticParams.mialgia);
    patientInfo.contact_infected = utils.toBool(diagnosticParams["contacto-covid19"]);
    patientInfo.works_in_health = false;

    patientInfo.country = "Venezuela";
    patientInfo.city = diagnosticParams.ciudad;
    patientInfo.municipality = diagnosticParams.munipicio;

    //Fill in diagnostic_venemergencias information
    patientInfo.email = venemParams.email;
    patientInfo['full_name'] = venemParams['full_name'];
    patientInfo.nid = venemParams.nid;
    patientInfo.age = venemParams.age;
    patientInfo.gender = venemParams.gender;
    patientInfo['phone_number'] = venemParams['phone_number'];
    patientInfo['health_worker'] = venemParams['health_worker'];

    //TODO(alberto)
    patientInfo.comorbility = [];
    patientInfo.othersSymptomsVirus = [];

    return patientInfo;
}

function handle(agent) {
    //Get diagnosis
    var diagnosticParams;
    for (let i = 0; i < agent.contexts.length; i++) {
        if (agent.contexts[i].name === 'diagnostic') {
            diagnosticParams = agent.contexts[i];
            break;
        }
    }
    if (diagnosticParams === null) {
        console.error("Unable to fetch diagnostic context")
    }


    if (diagnosticParams.name != 'diagnostic') {
        console.error('Wrong context fetched!')
        return
    }
    const diagnosis = diagnostico.getDiagnosis(diagnosticParams.parameters);
    const patientInfo = getUserDetails(diagnosticParams.parameters, agent.parameters);

    patientInfo.health_risk = diagnosis[0][0];
    patientInfo.risk_of_infection = diagnosis[1][0];

    send(agent, patientInfo);
    agent.add(
        "Gracias por la información! En breve recibirás una llamada de Venemergencia"
    );
}

function send(agent, patientInfo) {

    patientInfo.origin = 'c4v'

    const url = config.get("venemergencias.url")
    fetch(url, {
        method: "POST",
        body: JSON.stringify(patientInfo),
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
        },
    }).then((res) => console.log(res));
}

exports.handle = handle;
