const diagnostico = require('./diagnostico');

function generateCases(options) {
    let result = [];

    function recurse(option, builtStr) {
        if (option == options.length) {
            result.push(builtStr);
            return;
        }

        ["si", "no"].forEach(function(item, index) {
            let temp = builtStr.concat([item]);
            let nopt = option + 1;
            recurse(nopt, temp);
        });

        return builtStr;
    }
    recurse(0, []);
    return result;
}

let options = [
    'contacto-covid19', 
    'fiebre',
    'tos',
    'anosmia-rash',
    'disnea-confusion',
    'cefalea',
    'mialgia-garganta',
    'fatiga-hiporexia', 
    'dolor-pecho'];

let covidCases = generateCases(options).map(x =>
    x.map(function(e, i) {
        return [options[i], e];
    })
);

let min = 0;
let max = 0;
let bajos = 0;
let medios = 0;
let altos = 0;
covidCases.forEach(function(testCase, index) {
    let vars = [];
    testCase.forEach(function(item, index) {
        vars.push(diagnostico.getWeight(item[1], item[0]));
    });

    let covid = diagnostico.getRiesgoInfeccion([
        vars[0],
        vars[1],
        vars[2],
        vars[3],
        vars[4],
        vars[5],
        vars[6],
        vars[7],
        vars[8]
    ]);

    max = Math.max(max, covid[1]);
    min = Math.min(min, covid[1]);

    switch (covid[0]) {
        case "BAJO":
            bajos += 1;
            break;
        case "MEDIO":
            medios += 1;
            break;
        case "ALTO":
            altos += 1;
            break;
    }
});

test('Min score should be 0', () => {
    expect(min).toBe(0);
});

test('Max score should be 41', () => {
    expect(max).toBe(41);
});

test('There should be 453 ALTOS', () => {
    expect(altos).toBe(453);
});

test('There should be 40 MEDIOS', () => {
    expect(medios).toBe(40);
});

test('There should be 19 BAJOS', () => {
    expect(bajos).toBe(19);
});