const fetch = require("node-fetch");
const diagnostico = require('./diagnostico');
const config = require('config');


function sendToAngostura(agent, rMedico, rCovid, rScore) {
    const raw_payload = agent.parameters;
    raw_payload.user = {};

    const request = agent.request_;

    if (
        request &&
        "body" in request &&
        "originalDetectIntentRequest" in request.body
    ) {
        const reqbody = request.body.originalDetectIntentRequest;
        console.log(reqbody);
        if ("source" in reqbody) {
            raw_payload.user.source = reqbody.source;
        }
        if (
            "payload" in reqbody &&
            "data" in reqbody.payload &&
            "from" in reqbody.payload.data &&
            "id" in reqbody.payload.data.from
        ) {
            raw_payload.user.user_id = reqbody.payload.data.from.id;
        }
    }

    // Send risk assessment
    raw_payload.riesgo_medico = rMedico;
    raw_payload.riesgo_covid = rCovid;
    raw_payload.riesgo_total = rScore;

    const objJsonStr = JSON.stringify(raw_payload);
    const objJsonB64 = Buffer.from(objJsonStr).toString("base64");

    const angosturaPayload = {
        type: "covid_bot_diagnostic",
        version: "1",
        payload: objJsonB64
    };

    // Send events to both prod & dev while we migrate.
    // TODO(gustavo): Parametrize URL and use only the desired one.
    const url = config.get("angostura.url")
    fetch(url, {
        method: "POST",
        body: JSON.stringify(angosturaPayload),
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
        }
    }).then(res => console.log(res));
}

function telemedResp() {
    return 'Te recomendamos que te aisles en caso de sospecha de tener el virus.\n \
    ¿Deseas consultar con un médico? Necesitaremos información adicional para \
    que recibas una consulta gratuita de uno de nuestros médicos\n \
    Escribe "proceder" para continuar';
}

function buildResp(med, covid) {
    let message = "¡Gracias por responder!\n";
    message += "Tu riesgo de tener COVID-19 es " + covid + ".\n";
    message += "Tu riesgo para una infección severa es " + med + ".\n";
    message += "En caso de presentar nuevos síntomas en 48h, vuelve a chequearte.";

    // Send to telemedicina if any risk is higher than low
    if (covid == "MEDIO" || covid == "ALTO")
        {
            /*
                Supported cases:
                (medio, bajo)
                (medio, medio)
                (medio, alto)
                (alto, bajo)
                (alto, medio)
                (alto, alto)
            */
            message += telemedResp();
        }
        
    return message;
}

function handle(agent) {
    //Get diagnosis
    const diagnosis = diagnostico.getDiagnosis(agent.parameters);
    const medicalRisk = diagnosis[0];
    const covidRisk = diagnosis[1];

    //Execute actions: return response and send to proxy
    const rScore = medicalRisk[1] + covidRisk[1];
    sendToAngostura(agent, medicalRisk[1], covidRisk[1], rScore);
    agent.add(buildResp(medicalRisk[0], covidRisk[0]));
}

exports.handle = handle;