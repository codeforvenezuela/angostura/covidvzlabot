const config = {
  angostura: {
    url: 'https://us-central1-event-pipeline.cloudfunctions.net/prod-angosturagate'
  },
  venemergencias: {
      url: 'https://www.venemergencia.com/api/covid19/c4v',
  },
  sostelemed: {
    init_session_url: 'https://llamadasos.ucv.ve/chatbot/api/initSession',
    chatbot_url: 'https://llamadasos.ucv.ve/chatbot/api/PluginCustomchatbotIndex',
    end_session_url: 'https://llamadasos.ucv.ve/chatbot/api/killSession'
  }
};

module.exports = config;

