# CovidBotVzla

<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)

<!-- ABOUT THE PROJECT -->
## About The Project

This project implements a conversational chat bot to help people understand
if they might be at risk of having covid-19 and if they are at risk of
further complications in case of having the disease.

It was built under the guidance of [@medicosxlasalud](https://twitter.com/medicosxlasalud).

For more information about the project see our [Notion page](https://www.notion.so/codeforvenezuela/Covid19Bot-34fbbf36bcc44c90b3e12821f7b2bd6f).

### Built With

* [Node](https://nodejs.org/en/)
* [Dialogflow](https://dialogflow.cloud.google.com/)
* [Semantic Release](https://github.com/semantic-release/semantic-release)

<!-- GETTING STARTED -->
## Getting Started

Just run `npm run dev` to fire up the test server.

### Prerequisites

* npm
```sh
npm install npm@latest -g
```

### Installation

1. Clone the repo
```sh
git clone https://gitlab.com/codeforvenezuela/angostura/covidvzlabot.git
```
2. Install NPM packages
```sh
npm install
```

<!-- USAGE EXAMPLES -->
## Usage


<!-- ROADMAP -->
## Roadmap

See the [open issues](https://www.notion.so/codeforvenezuela/Covid19Bot-34fbbf36bcc44c90b3e12821f7b2bd6f) for a list of proposed features (and known issues).


<!-- CONTRIBUTING -->
## Contributing

Contributions are welcomed and **appreciated**:

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request

<!-- LICENSE -->
## License

All Code4Venezuela projects are distributed under the MIT License. See
`LICENSE` for more information.
