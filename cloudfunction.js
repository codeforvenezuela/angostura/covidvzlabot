const functions = require('firebase-functions')
const app = require('./src/app.js')

exports.app = functions.https.onRequest(app)
